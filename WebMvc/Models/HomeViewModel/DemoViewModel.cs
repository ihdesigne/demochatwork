﻿namespace WebMvc.Models.HomeViewModel
{
    using DbLayer.Models;
    using System.Collections.Generic;

    public class DemoViewModel
    {
        public DemoViewModel()
        {
            drivers = new List<DriverRoom>();
        }
        public IEnumerable<DriverRoom> drivers { get; set; }
    }
}