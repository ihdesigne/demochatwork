﻿namespace WebMvc.Models.HomeViewModel
{
    using DbLayer.EF;
    using System.Collections.Generic;

    public class IndexViewModel
    {
        public IndexViewModel()
        {
            agents = new List<Agent>();
        }
        public IEnumerable<Agent> agents { get; set; }
    }

}