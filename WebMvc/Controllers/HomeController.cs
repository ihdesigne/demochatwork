﻿namespace WebMvc.Controllers
{
    using ChatworkAPI;
    using DbLayer.Models;
    using System;
    using System.Text;
    using System.Web.Mvc;
    using WebMvc.Models.HomeViewModel;

    public class HomeController : Controller
    {


        public ActionResult Index()
        {
            var res = new IndexViewModel();
            res.agents = new AgentDao().GetAgents();
            return View(res);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult DemoChatwork(long id)
        {
            var res = new DemoViewModel();
            res.drivers = new AgentDao().GetDrivers(id);
            
            return View(res);
        }

        public JsonResult MentionMessage(string roomId)
        {
            var res = true;
            try
            {
                var token = "660a8f3e04e7d93cb8371a6cce657b00";
                var client = new ChatworkClient(token);
                var url = "https://www.chatwork.com/#!rid";
                var ducRoomId = "196188325";

                var rooms = client.GetRooms();

                var messageBody = new StringBuilder();
                messageBody.Append($"{url}{roomId}");
                client.SendMessage(ducRoomId, messageBody.ToString());
            }
            catch(Exception ex)
            {
                res = false;
            }
            
            return new JsonResult { Data = res };
        }

        public ActionResult Demo2()
        {
            return View();
        }
    }
}