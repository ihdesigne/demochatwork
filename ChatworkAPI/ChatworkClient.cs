﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatworkAPI.Communicators;
using ChatworkAPI.Models;

namespace ChatworkAPI
{
    /// <summary>
    /// Chatwork Client
    /// </summary>
    public class ChatworkClient
    {
        private readonly ApiCommunicator _api;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="apiToken"></param>
        public ChatworkClient(string apiToken)
        {
            if (string.IsNullOrEmpty(apiToken)) throw new ArgumentException($"{nameof(apiToken)} must not be NullOrEmpty.");
            _api = new ApiCommunicator(apiToken);
        }

        /// <summary>
        /// Endpoint Me theo document chatwork
        /// </summary>
        /// <returns></returns>
        public Me GetMe()
        {
            return _api.Get<Me>(@"me");
        }

        /// <summary>
        /// Endpoint Me theo document chatwork
        /// </summary>
        /// <returns></returns>
        public MyStatus GetStatus()
        {
            return _api.Get<MyStatus>(@"my/status");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assigned_by_account_id"></param>
        /// <param name="statuses"></param>
        /// <returns></returns>
        public IEnumerable<MyTask> GetTasks(string assigned_by_account_id, IEnumerable<string> statuses)
        {
            if (statuses == null)
            {
                throw new ArgumentNullException("statuses");
            }
            if (!statuses.Any())
            {
                throw new InvalidOperationException("statuses is empty(add open/close).");
            }

            var parameters = new Dictionary<string, object>
            {
                {"assigned_by_account_id", assigned_by_account_id},
                {@"status", string.Join(",", statuses)}
            };

            return _api.Get<IEnumerable<MyTask>>(@"my/tasks");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Contact> GetContacts()
        {
            return _api.Get<IEnumerable<Contact>>(@"contacts");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Room> GetRooms()
        {
            return _api.Get<IEnumerable<Room>>(@"rooms");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public Room GetRoom(string roomId)
        {
            if (string.IsNullOrEmpty(roomId)) throw new ArgumentException($"{nameof(roomId)} must not be NullOrEmpty.");

            return _api.Get<Room>($"rooms/{roomId}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public IEnumerable<Member> GetRoomMembers(string roomId)
        {
            if (string.IsNullOrEmpty(roomId)) throw new ArgumentException($"{nameof(roomId)} must not be NullOrEmpty.");

            return _api.Get<IEnumerable<Member>>($"rooms/{roomId}/members");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="messageBody"></param>
        /// <returns></returns>
        public ResponseMessage SendMessage(string roomId, string messageBody)
        {
            if (string.IsNullOrEmpty(roomId)) throw new ArgumentException($"{nameof(roomId)} must not be NullOrEmpty.");

            var parameters = new Dictionary<string, object>
            {
                {"body", messageBody},
            };

            return _api.Post<ResponseMessage>($"rooms/{roomId}/messages", parameters);
        }
    }
}
