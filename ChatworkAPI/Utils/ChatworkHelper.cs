﻿using ChatworkAPI.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ChatworkAPI.Utils
{
    /// <summary>
    /// Class helper
    /// </summary>
    public static class ChatworkHelper
    {
        /// <summary>
        /// Xây dựng request với danh sách params
        /// </summary>
        /// <param name="request"></param>
        /// <param name="parameters"></param>
        public static void BuildParameters(RestRequest request, Dictionary<string, object> parameters)
        {
            if (parameters == null || !parameters.Any()) return;

            foreach (var parameter in parameters)
            {
                request.AddParameter(parameter.Key, parameter.Value);
            }
        }

        /// <summary>
        /// custom throw exception
        /// </summary>
        /// <param name="response"></param>
        public static void ThrowExceptionIfNeed(IRestResponse response)
        {
            var rl = new RateLimit(response);
            if ((int)response.StatusCode == 429)
                throw new TooManyRequestsException(rl);

            if (response.StatusCode != HttpStatusCode.NoContent && (int)response.StatusCode != 200)
                throw new Exception($"API returns {response.StatusCode}. API response is here: {response.Content}");
        }
    }
}
