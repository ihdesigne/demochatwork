﻿using ChatworkAPI.Utils;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace ChatworkAPI.Communicators
{
    internal class ApiCommunicator
    {
        private static string BaseUri => @"https://api.chatwork.com/v2/";
        protected string ApiToken { get; }

        public ApiCommunicator(string apiToken)
        {
            if (string.IsNullOrWhiteSpace(apiToken))
            {
                throw new ArgumentNullException(apiToken);
            }
            ApiToken = apiToken;
        }

        public T Get<T>(string resource, Dictionary<string, object> parameters = null)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri(BaseUri + resource)
            };

            var request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
            };

            request.AddHeader("X-ChatWorkToken", ApiToken);

            ChatworkHelper.BuildParameters(request, parameters);

            var response = client.Execute(request);

            ChatworkHelper.ThrowExceptionIfNeed(response);

            return JsonConvert.DeserializeObject<T>(response.Content);
        }

        public T Post<T>(string resource, Dictionary<string, object> parameters)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri(BaseUri + resource)
            };
            var request = new RestRequest(Method.POST)
            {
                RequestFormat = DataFormat.Json,
            };
            request.AddHeader("X-ChatWorkToken", ApiToken);

            ChatworkHelper.BuildParameters(request, parameters);

            var response = client.Execute(request);

            ChatworkHelper.ThrowExceptionIfNeed(response);

            return JsonConvert.DeserializeObject<T>(response.Content);
        }

        public T Send<T>(string resource, Dictionary<string, object> parameters, Method method)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri(BaseUri + resource)
            };
            var request = new RestRequest(method)
            {
                RequestFormat = DataFormat.Json,
            };
            request.AddHeader("X-ChatWorkToken", ApiToken);

            ChatworkHelper.BuildParameters(request, parameters);

            var response = client.Execute(request);

            ChatworkHelper.ThrowExceptionIfNeed(response);

            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
