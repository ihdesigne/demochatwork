﻿using System;
using Newtonsoft.Json;

namespace ChatworkAPI.Models
{
    class TooManyRequestsException : Exception
    {
        /// <summary>
        /// RateLimit
        /// </summary>
        [JsonProperty("rate_limit")]
        public RateLimit RateLimit { get; private set; }

        /// <summary>
        /// TooManyRequestsException
        /// </summary>
        /// <param name="rateLimit">RateLimit</param>
        public TooManyRequestsException(RateLimit rateLimit)
        {
            RateLimit = rateLimit;
        }

        /// <summary>
        /// formatting members
        /// </summary>
        public override string ToString()
        {
            return $"{base.ToString()}, RateLimit: {RateLimit}";
        }
    }
}
