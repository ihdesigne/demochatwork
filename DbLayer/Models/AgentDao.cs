﻿using DbLayer.EF;
using System.Collections.Generic;
using System.Linq;

namespace DbLayer.Models
{
    public class AgentDao
    {
        private DemoChatworkDBContext context = null;

        public AgentDao()
        {
            context = new DemoChatworkDBContext();
        }

        public IEnumerable<Agent> GetAgents()
        {
            IQueryable<Agent> model = context.Agents;
            model = context.Agents;
            return model.ToList();
        }

        public IEnumerable<DriverRoom> GetDrivers(long agentId)
        {
            var result = from d in context.Drivers
                         join r in (context.Rooms.Where(x => x.AgentId == agentId))
                         on d.Id equals r.DriverId
                         select new DriverRoom {
                             driver = d,
                             roomId = r.RoomId
                         };
            return result.ToList();
        }

    }
}
