﻿namespace DbLayer.Models
{
    using DbLayer.EF;

    public class DriverRoom
    {
        public DriverRoom()
        {
            driver = new Driver();
        }
        public Driver driver { get; set; }
        public string roomId { get; set; }
    }
}
