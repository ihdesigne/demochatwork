namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agents",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 500),
                        AgentId = c.Long(),
                        DriverId = c.Long(),
                        RoomId = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agents", t => t.AgentId)
                .ForeignKey("dbo.Drivers", t => t.DriverId)
                .Index(t => t.AgentId)
                .Index(t => t.DriverId);
            
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rooms", "DriverId", "dbo.Drivers");
            DropForeignKey("dbo.Rooms", "AgentId", "dbo.Agents");
            DropIndex("dbo.Rooms", new[] { "DriverId" });
            DropIndex("dbo.Rooms", new[] { "AgentId" });
            DropTable("dbo.Drivers");
            DropTable("dbo.Rooms");
            DropTable("dbo.Agents");
        }
    }
}
