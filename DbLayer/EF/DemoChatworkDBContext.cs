namespace DbLayer.EF
{
    using System.Data.Entity;

    public partial class DemoChatworkDBContext : DbContext
    {
        public DemoChatworkDBContext()
            : base("name=DemoChatworkDBContext")
        {
        }

        public virtual DbSet<Agent> Agents { get; set; }
        public virtual DbSet<Driver> Drivers { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
