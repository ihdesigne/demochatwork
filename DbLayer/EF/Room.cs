namespace DbLayer.EF
{
    using System.ComponentModel.DataAnnotations;

    public partial class Room
    {
        public long Id { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        public long? AgentId { get; set; }

        public long? DriverId { get; set; }

        public virtual Agent Agent { get; set; }

        public virtual Driver Driver { get; set; }

        [StringLength(50)]
        public string RoomId { get; set; }
    }
}
